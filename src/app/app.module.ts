import { NgModule } from '@angular/core';
import { IonicApp, IonicModule } from 'ionic-angular';

// Home 1 Files
//import { MyApp } from './home-one/app.component'
import { TabsPage } from '../pages/tabs/tabs';

// Home 2 Files
import { MyApp } from './home-three/app.component';

// --- Theme Pages --- //
import { HomeOne } from '../pages/home-one/home-one';
import { HomeTwo } from '../pages/home-two/home-two';

import { HomeThree } from '../pages/home-three/home-three';
import { TabsHome3 } from '../pages/tabs-home-3/tabs-home-3';

import { Login } from '../pages/auth/login/login';
import { Register } from '../pages/auth/register/register';
import { Otp } from '../pages/auth/otp/otp';

// Shopping Pages //
import { ExclusiveProducts } from '../pages/shopping/exclusive-products/exclusive-products';

// Recharge Pages //
import { MyModal } from '../pages/my-modal/my-modal';
import { Prepaid } from '../pages/recharge/prepaid/prepaid';

@NgModule({
  declarations: [
  MyApp,
  HomeOne,
  HomeTwo,
  HomeThree,
  MyModal,
  TabsPage,
  TabsHome3,
  ExclusiveProducts,
  Login,
  Register,
  Otp,
  Prepaid,
  ],
  imports: [
  IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
  MyApp,
  HomeOne,
  HomeTwo,
  HomeThree,
  MyModal,
  TabsPage,
  TabsHome3,
  ExclusiveProducts,
  Login,
  Register,
  Otp,
  Prepaid,
  ],
  providers: []
})
export class AppModule {}
