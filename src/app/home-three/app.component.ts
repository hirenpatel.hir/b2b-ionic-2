import { Component, ViewChild } from '@angular/core';
import { Platform, MenuController, Nav } from 'ionic-angular';
import { StatusBar } from 'ionic-native';

// import { HomeThree } from '../../pages/home-three/home-three';
import { TabsHome3 } from '../../pages/tabs-home-3/tabs-home-3';
import { Login } from '../../pages/auth/login/login';
// import { Register } from '../../pages/auth/register/register';

@Component({
  templateUrl: 'app.html'
})

export class MyApp {
  login:any;
  home:any;
  @ViewChild(Nav) nav: Nav;
  // rootPage = HomeOne;
  rootPage = TabsHome3;

  // rootPage: any = Login;
  pages: Array<{title: string, component: any}>;

  constructor( public platform: Platform, public menu: MenuController)
  {
    this.login = Login;
    this.home = TabsHome3;
    this.initializeApp();
  }



  initializeApp() {
    this.platform.ready().then(() =>{
      StatusBar.backgroundColorByHexString("#3142a0");
    });
  }

  gotoLogin(){
    this.menu.close();
    this.nav.setRoot(Login);
  }

  gotoHome(){
    this.menu.close();
    this.nav.setRoot(TabsHome3);
  }

  openPage(page) {
    // close the menu when clicking a link from the menu
    this.menu.close();
    // navigate to the new page if it is not the current page
    this.nav.setRoot(page.component);
  }

  // constructor(platform: Platform) {
    //   platform.ready().then(() => {
      //     // Okay, so the platform is ready and our plugins are available.
      //     // Here you can do any higher level native things you might need.
      //     StatusBar.styleDefault();
      //     Splashscreen.hide();
      //   });
      // }
    }
