import { Component, ViewChild } from '@angular/core';
import { Platform, MenuController, Nav } from 'ionic-angular';
import { StatusBar } from 'ionic-native';

// import { HomeOne } from '../pages/home-one/home-one';
// import { HomeThree } from '../pages/home-three/home-three';
// import { Login } from '../../pages/auth/login/login';
import { Register } from '../../pages/auth/register/register';
// import { AboutPage } from '../pages/about/about';
// import { ContactPage } from '../pages/contact/contact';

@Component({
  templateUrl: './home-three/app.html'
  // templateUrl: '../pages/auth/login/login.html'
})


export class MyApp {
  @ViewChild(Nav) nav: Nav;

  // rootPage = HomeOne;
  rootPage = Register;

  // rootPage: any = Login;
  pages: Array<{title: string, component: any}>;

  constructor(
              public platform: Platform,
              public menu: MenuController
              ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() =>{
      StatusBar.backgroundColorByHexString('#3142a0');
    });
  }

  openPage(page) {
    // close the menu when clicking a link from the menu
    this.menu.close();
    // navigate to the new page if it is not the current page
    this.nav.setRoot(page.component);
  }

  // constructor(platform: Platform) {
    //   platform.ready().then(() => {
      //     // Okay, so the platform is ready and our plugins are available.
      //     // Here you can do any higher level native things you might need.
      //     StatusBar.styleDefault();
      //     Splashscreen.hide();
      //   });
      // }
    }
