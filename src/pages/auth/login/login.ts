import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { Register } from '../register/register';

/*
  Generated class for the Login page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
  */
  @Component({
      selector: 'page-login',
      templateUrl: 'login.html'
  })
  export class Login {
      register:any;
      constructor(public navCtrl: NavController) {
          this.register = Register;
      }

      ionViewDidLoad() {
          console.log('Hello Login Page');
      }

  }
