import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { Otp } from '../otp/otp';
/*
  Generated class for the Register page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
  */
  @Component({
      selector: 'page-register',
      templateUrl: 'register.html'
  })
  export class Register {
      otp:any;
      constructor(public navCtrl: NavController) {
          this.otp = Otp;
      }

      ionViewDidLoad() {
          console.log('Hello Register Page');
      }

  }
