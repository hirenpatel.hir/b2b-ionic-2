import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

/*
  Generated class for the Gas page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-gas',
  templateUrl: 'gas.html'
})
export class Gas {

  constructor(public navCtrl: NavController) {}

  ionViewDidLoad() {
    console.log('Hello Gas Page');
  }

}
