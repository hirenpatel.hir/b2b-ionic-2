import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

/*
  Generated class for the Postpaid page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-postpaid',
  templateUrl: 'postpaid.html'
})
export class Postpaid {

  constructor(public navCtrl: NavController) {}

  ionViewDidLoad() {
    console.log('Hello Postpaid Page');
  }

}
