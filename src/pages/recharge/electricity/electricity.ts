import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

/*
  Generated class for the Electricity page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-electricity',
  templateUrl: 'electricity.html'
})
export class Electricity {

  constructor(public navCtrl: NavController) {}

  ionViewDidLoad() {
    console.log('Hello Electricity Page');
  }

}
