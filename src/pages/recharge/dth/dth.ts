import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

/*
  Generated class for the Dth page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-dth',
  templateUrl: 'dth.html'
})
export class Dth {

  constructor(public navCtrl: NavController) {}

  ionViewDidLoad() {
    console.log('Hello Dth Page');
  }

}
