import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

/*
  Generated class for the Prepaid page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-prepaid',
  templateUrl: 'prepaid.html'
})
export class Prepaid {

  constructor(public navCtrl: NavController) {}

  ionViewDidLoad() {
    console.log('Hello Prepaid Page');
  }

}
