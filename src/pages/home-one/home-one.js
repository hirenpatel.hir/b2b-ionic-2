var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { StatusBar } from 'ionic-native';
import { Platform, NavController, ModalController } from 'ionic-angular';
import { MyModal } from '../my-modal/my-modal';
/*
  Generated class for the HomeOne page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
  */
export var HomeOne = (function () {
    function HomeOne(platform, navCtrl, modalCtrl) {
        this.platform = platform;
        this.navCtrl = navCtrl;
        this.modalCtrl = modalCtrl;
    }
    HomeOne.prototype.ionViewDidLoad = function () {
        console.log('Hello Profile Page');
        this.platform.ready().then(function () {
            StatusBar.backgroundColorByHexString("#053b54");
        });
    };
    HomeOne.prototype.showModal = function () {
        var modal = this.modalCtrl.create(MyModal);
        modal.onDidDismiss(function (data) {
            // console.log(data);
        });
        modal.present();
    };
    HomeOne = __decorate([
        Component({
            selector: 'page-home-one',
            templateUrl: 'home-one.html'
        }), 
        __metadata('design:paramtypes', [Platform, NavController, ModalController])
    ], HomeOne);
    return HomeOne;
}());
//# sourceMappingURL=home-one.js.map