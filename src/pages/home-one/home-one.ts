import { Component } from '@angular/core';
import { StatusBar } from 'ionic-native';
import { Platform, NavController, ModalController } from 'ionic-angular';

import { MyModal } from '../my-modal/my-modal';

/*
  Generated class for the HomeOne page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
  */
  @Component({
    selector: 'page-home-one',
    templateUrl: 'home-one.html'
  })
  export class HomeOne {

    constructor(public platform: Platform, public navCtrl: NavController, public modalCtrl: ModalController) {}

    ionViewDidLoad() {
      this.platform.ready().then(() =>{
        StatusBar.backgroundColorByHexString("#2b3d9c");
      })
    }

    showModal() {
      let modal = this.modalCtrl.create(MyModal);
      modal.onDidDismiss(data=>{
        // console.log(data);
      });
      modal.present();
    }

    mySlideOptions = {
      autoplay:  2000,
      initialSlide: 1,
      pager:  true,
      loop: false
    };

  }
