import { Component } from '@angular/core';
import { NavController,ViewController } from 'ionic-angular';

/*
  Generated class for the MyModal page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
  */
  @Component({
      selector: 'page-my-modal',
      templateUrl: 'my-modal.html'
  })
  export class MyModal {

      constructor(public navCtrl: NavController, public viewCtrl: ViewController) {}

      ionViewDidLoad() {
          console.log('Hello MyModal Page');
      }
      dismiss(){
          this.viewCtrl.dismiss({username: 'Works'});
      }

  }
