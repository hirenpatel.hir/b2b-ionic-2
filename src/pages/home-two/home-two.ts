import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { ExclusiveProducts } from '../shopping/exclusive-products/exclusive-products';
/*
  Generated class for the HomeTwo page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
  */
  @Component({
      selector: 'page-home-two',
      templateUrl: 'home-two.html'
  })
  export class HomeTwo {
      exclusiveProducts: any;
      constructor(public navCtrl: NavController)
      {
          this.exclusiveProducts = ExclusiveProducts;
      }

      ionViewDidLoad() {
          console.log('Hello HomeTwo Page');
      }

      mySlideOptions = {
          autoplay:  2000,
          initialSlide: 1,
          pager:  true,
          loop: false
      };

  }
