import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { ExclusiveProducts } from '../shopping/exclusive-products/exclusive-products';
import { Prepaid } from '../recharge/prepaid/prepaid';
/*
  Generated class for the HomeThree page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
  */
  @Component({
    selector: 'page-home-three',
    templateUrl: 'home-three.html'
  })
  export class HomeThree {
    exclusiveProducts: any;
    prepaid: any;
    tab1Root: any = ExclusiveProducts;
    tab2Root: any = ExclusiveProducts;
    constructor(public navCtrl: NavController) {
      this.exclusiveProducts = ExclusiveProducts;
      this.prepaid = Prepaid;
    }
    mySlideOptions = {
      autoplay:  2000,
      initialSlide: 1,
      loop: false
    };
  }
