import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';


import { HomeThree } from '../home-three/home-three';
import { ExclusiveProducts } from '../shopping/exclusive-products/exclusive-products';

/*
  Generated class for the TabsHome3 page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
  */
  @Component({
    selector: 'page-tabs-home-3',
    templateUrl: 'tabs-home-3.html'
  })
  export class TabsHome3 {
    tab1Root: any = HomeThree;
    tab2Root: any = ExclusiveProducts;
    constructor(public navCtrl: NavController) {}

    ionViewDidLoad() {
      console.log('Hello TabsHome3 Page');
    }

  }
