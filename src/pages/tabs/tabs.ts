import { Component } from '@angular/core';

import { HomeOne } from '../home-one/home-one';
import { ExclusiveProducts } from '../shopping/exclusive-products/exclusive-products';

@Component({
    templateUrl: 'tabs.html'
})
export class TabsPage {
    // this tells the tabs component which Pages
    // should be each tab's root Page
    tab1Root: any = HomeOne;
    tab2Root: any = ExclusiveProducts;

    constructor() {

    }
}
